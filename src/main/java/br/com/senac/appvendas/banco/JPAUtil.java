
package br.com.senac.appvendas.banco;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPAUtil {
    
    private static final EntityManagerFactory emf = Persistence.createEntityManagerFactory("LivrariaPU");
    
    
    public static EntityManager getEntityManager(){
        
        try{
            return emf.createEntityManager() ; 
        }catch(Exception ex){
            ex.printStackTrace();
            throw new RuntimeException("Falha ao acessar o banco.");
        }
        
    }
    
    
}
